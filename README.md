# AnnRG - An artificial neural network solute geothermometer

## Name
AnnRG (Artificial neural network Regression Geothermometer)

## Description
An artificial neural network solute geothermometer for geothermal reservoir temperature prediction

## Function
Supervised feedforward multilayer perceptron solving the regression analysis of fluid chemistry to predict reservoir temperature.

## Installation
AnnRG is based on Python 3.8.5 and additional Python libraries: _NumPy, pandas, Matplotlib, seaborn, scikit-learn, TensorFlow, Keras_

## Usage
INPUT: cvs-file:['pH','Na','K','Ca','Mg','SiO2','Cl','Temperature']

OUTPUT: graphical output of predicted vs. measured data, and error diagrams/values

## Support
lars.ystroem@kit.edu

## Authors
Vollmer, M.; Ystroem, L.H.

## License
MIT License
